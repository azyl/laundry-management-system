
FROM maven:3.6.3-openjdk-14-slim as maven_build
COPY . /project
RUN  cd /project && mvn clean package -Dactive.profile=default
RUN  mv /project/target/*.jar /app.jar

FROM openjdk:14-alpine
RUN addgroup -S spring && adduser -S spring -G spring
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.7.3/wait /wait
RUN chmod +x /wait
USER spring:spring
COPY --from=maven_build /*jar /

ENTRYPOINT ["java","-jar","/app.jar"]
#CMD ["./wait-for-it.sh" , "http://keycloak:8080/auth/realms/dev" , "--strict" , "--timeout=300" , "--" , "java -jar /app.jar"]
