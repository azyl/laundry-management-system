INSERT INTO TENANT(ID, NAME, CREDENTIALS)
values ('515d9a28-5ae8-40f2-a6cf-c22b0bcf5425', 'test_tenant', 'test_cred');

insert INTO APPLIANCE(ID,
                      TENANT_ID,
                      LAST_MODIFIED,
                      CREATED_DATE,
                      TYPE,
                      NAME,
                      DESCRIPTION,
                      WASHING_SPECIFIC_ATTRIBUTE)
values ('7affdc37-beb4-40a0-b566-aca94f0f7ce9',
        '515d9a28-5ae8-40f2-a6cf-c22b0bcf5425',
        CURRENT_TIMESTAMP,
        CURRENT_TIMESTAMP,
        'WASHING_APPLIANCE',
        'testing appliance 1',
        'test appliance description',
        'washing specific text');

insert INTO APPLIANCE(ID,
                      TENANT_ID,
                      LAST_MODIFIED,
                      CREATED_DATE,
                      TYPE,
                      NAME,
                      DESCRIPTION,
                      DRYING_SPECIFIC_ATTRIBUTE)
values ('2db6f541-404e-467f-90df-9215dc838383',
        '515d9a28-5ae8-40f2-a6cf-c22b0bcf5425',
        CURRENT_TIMESTAMP,
        CURRENT_TIMESTAMP,
        'DRYING_APPLIANCE',
        'testing appliance 2',
        'test appliance description',
        'drying specific text');
