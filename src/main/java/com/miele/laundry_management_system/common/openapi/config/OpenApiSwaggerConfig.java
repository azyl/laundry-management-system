package com.miele.laundry_management_system.common.openapi.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.OAuthFlow;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class OpenApiSwaggerConfig {

  @Value("${springdoc.oauth2.client.provider.keycloak.authorization-uri}")
  private String authorizationUrl;

  @Value("${springdoc.oauth2.client.provider.keycloak.token-uri}")
  private String tokenUrl;

  @Value("${springdoc.oauth2.client.registration.dev.redirect-uri}")
  private String refreshUrl;

  @Bean
  @Profile("!jwt-only")
  public OpenAPI api() {
    Map<String, Object> extensions = new HashMap<>();
    extensions.put("x-keywords", "LMS");
    extensions.put("x-related-masterdata", "washing");
    extensions.put("x-solution", "LMS");
    extensions.put("x-scope", "miele");
    License license = new License();
    license.setName("Apache License Version 2.0");
    license.setUrl("https://www.apache.org/licenses/LICENSE-2.0");
    Contact contact = new Contact();
    contact.setName("Miele LMS Team");
    contact.setEmail("lms@miele.ro");
    contact.addExtension("x-teams", "LMS");

    var openApi =
        new OpenAPI()
            .info(
                new Info()
                    .title("Laundry management service")
                    .description("REST API for Miele Laundry management systems")
                    .version("0.0.1")
                    .termsOfService("http://terms-of-services.url")
                    .license(license)
                    .contact(contact)
                    .extensions(extensions));

    openApi.components(
        new Components()
            .addSecuritySchemes(
                "KeycloakOauth2Security",
                new io.swagger.v3.oas.models.security.SecurityScheme()
                    .type(io.swagger.v3.oas.models.security.SecurityScheme.Type.OAUTH2)
                    .in(io.swagger.v3.oas.models.security.SecurityScheme.In.HEADER)
                    .bearerFormat("jwt")
                    .flows(
                        new io.swagger.v3.oas.models.security.OAuthFlows()
                            //
                            // .clientCredentials(
                            //                                                                new
                            // io.swagger.v3.oas.models.security.OAuthFlow()
                            //
                            //  .authorizationUrl(authorizationUrl)
                            //
                            //  .tokenUrl(tokenUrl)
                            //
                            // .scopes(new Scopes().addString("read","read"))
                            //                                                        )
                            .authorizationCode(
                                new OAuthFlow()
                                    .authorizationUrl(authorizationUrl)
                                    .tokenUrl(tokenUrl)
                                    .refreshUrl(refreshUrl)
                                //                                    .scopes(
                                //                                        new Scopes()
                                //                                            .addString("read",
                                // "read")
                                //                                            .addString("write",
                                // "write")
                                //                                            )
                                ))));

    return openApi;
  }

  @Bean
  @Profile("jwt-only")
  public OpenAPI customOpenAPI() {
    final String securitySchemeName = "bearerAuth";
    return new OpenAPI()
        .addSecurityItem(new SecurityRequirement().addList(securitySchemeName))
        .components(
            new Components()
                .addSecuritySchemes(securitySchemeName,
                    new SecurityScheme()
                        .name(securitySchemeName)
                        .type(SecurityScheme.Type.HTTP)
                        .scheme("bearer")
                        .bearerFormat("JWT")
                )
        )
        .info(new Info().title("LSM API").version("1").description("REST API for LAUNDRY MANAGEMENT SYSTEM"));
  }
}
