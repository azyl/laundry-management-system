package com.miele.laundry_management_system.common.security.config;

import com.miele.laundry_management_system.common.security.KeycloakLogoutHandler;
import com.miele.laundry_management_system.common.security.KeycloakOauth2UserService;
import org.springframework.core.convert.converter.Converter;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.security.oauth2.client.OAuth2ClientProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoderJwkSupport;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;
import org.springframework.web.client.RestTemplate;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig {

  private static final String CLAIM_ROLES = "roles";
  private static final String AUTHORITY_PREFIX = "ROLE_";

  @Bean
  public WebSecurityConfigurerAdapter webSecurityConfigurer(
      @Value("${kc.realm}") String realm, //
      KeycloakOauth2UserService keycloakOidcUserService, //
      KeycloakLogoutHandler keycloakLogoutHandler //
      ) {
    return new WebSecurityConfigurerAdapter() {
      @Override
      public void configure(HttpSecurity http) throws Exception {

        http
            .antMatcher("/**")
              .authorizeRequests()
            .antMatchers("/v3/api-docs/**", "/swagger-ui/**", "/swagger-ui.html")
              .permitAll()
            .antMatchers("/")
              .permitAll()
              .anyRequest()
              .authenticated()
            .and()
            //            .oauth2Login()
            //              .userInfoEndpoint()
            //              .oidcUserService(keycloakOidcUserService)
            //            .and()
            //            .and()
            //
            // .logout().invalidateHttpSession(true).addLogoutHandler(keycloakLogoutHandler)
            .oauth2ResourceServer()
              .jwt()
              .jwtAuthenticationConverter(getJwtAuthenticationConverter())
              .and()
            .and()
              .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        ;

        http.csrf().disable().cors().disable();
      }
    };
  }

  private Converter<Jwt, AbstractAuthenticationToken> getJwtAuthenticationConverter() {
    JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
    jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(
        getJwtGrantedAuthoritiesConverter());
    return jwtAuthenticationConverter;
  }

  private Converter<Jwt, Collection<GrantedAuthority>> getJwtGrantedAuthoritiesConverter() {
    JwtGrantedAuthoritiesConverter converter = new JwtGrantedAuthoritiesConverter();
    converter.setAuthoritiesClaimName(CLAIM_ROLES);
    converter.setAuthorityPrefix(AUTHORITY_PREFIX);
    return converter;
  }

  @Bean
  KeycloakOauth2UserService keycloakOidcUserService(OAuth2ClientProperties oauth2ClientProperties) {

    // TODO use default JwtDecoder - where to grab?
    NimbusJwtDecoderJwkSupport jwtDecoder =
        new NimbusJwtDecoderJwkSupport(
            oauth2ClientProperties.getProvider().get("keycloak").getJwkSetUri());

    SimpleAuthorityMapper authoritiesMapper = new SimpleAuthorityMapper();
    authoritiesMapper.setConvertToUpperCase(true);

    return new KeycloakOauth2UserService(jwtDecoder, authoritiesMapper);
  }

  @Bean
  public KeycloakLogoutHandler keycloakLogoutHandler() {
    return new KeycloakLogoutHandler(new RestTemplate());
  }
}
