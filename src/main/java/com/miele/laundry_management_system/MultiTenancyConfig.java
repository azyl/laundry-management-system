package com.miele.laundry_management_system;

import com.miele.laundry_management_system.model.common.transport.CustomHeaders;
import com.miele.laundry_management_system.model.tenant.multitenancy.FilterByTenant;
import com.miele.laundry_management_system.model.tenant.multitenancy.TenantContext;
import java.io.IOException;
import java.util.Optional;
import java.util.UUID;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MultiTenancyConfig {

  @Bean
  public FilterByTenant filterByTenant() {
    return new FilterByTenant();
  }

  @Bean
  public FilterRegistrationBean<TenantExtractionFilter> tenantExtractionFilter() {
    FilterRegistrationBean<TenantExtractionFilter> tenantFilter = new FilterRegistrationBean<>();
    tenantFilter.setFilter(new TenantExtractionFilter());
    tenantFilter.setOrder(0);
    return tenantFilter;
  }

  private static class TenantExtractionFilter extends HttpFilter {

    @Override
    protected void doFilter(
        HttpServletRequest request, HttpServletResponse response, FilterChain chain)
        throws IOException, ServletException {
      TenantContext.clear();
      Optional.ofNullable(request.getHeader(CustomHeaders.TENANT_ID_HEADER))
          .filter(tenantId -> !tenantId.isEmpty())
          .map(UUID::fromString)
          .ifPresent(TenantContext::setCurrentTenantId);

      chain.doFilter(request, response);
    }
  }
}
