package com.miele.laundry_management_system.model.tenant.service;

import com.miele.laundry_management_system.model.common.transport.Meta;
import com.miele.laundry_management_system.model.tenant.entity.Tenant;
import com.miele.laundry_management_system.model.tenant.repository.TenantRepository;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class TenantService {

  private final TenantRepository tenantRepository;

  public Page<Tenant> findAll(Meta meta) {
    return tenantRepository.findAll(PageRequest.of(meta.getPage(), meta.getPageSize()));
  }

  public Optional<Tenant> findById(UUID id) {
    return tenantRepository.findById(id);
  }

  public Tenant create(Tenant tenant) {
    tenant.setId(null);
    return tenantRepository.save(tenant);
  }

  public Tenant update(UUID id, Tenant tenant) {
    tenant.setId(id);
    return tenantRepository.save(tenant);
  }

  public void delete(UUID id) {
    tenantRepository.deleteById(id);
  }
}
