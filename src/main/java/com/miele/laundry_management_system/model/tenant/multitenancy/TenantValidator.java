package com.miele.laundry_management_system.model.tenant.multitenancy;

import com.miele.laundry_management_system.model.tenant.entity.Tenant;
import java.util.Optional;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PostLoad;

public class TenantValidator {

  @PostLoad
  public void validateTenant(Object entity) {
    if (!(entity instanceof TenantAccess)) {
      return;
    }

    TenantContext.getCurrentTenantId()
        .ifPresent(
            tenantId ->
                Optional.of(entity)
                    .map(TenantAccess.class::cast)
                    .map(TenantAccess::getTenant)
                    .map(Tenant::getId)
                    .filter(tenantId::equals)
                    .orElseThrow(
                        () ->
                            new EntityNotFoundException(
                                "Entity not available on the current tenant")));
  }
}
