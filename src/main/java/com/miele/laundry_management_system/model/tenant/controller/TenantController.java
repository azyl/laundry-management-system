package com.miele.laundry_management_system.model.tenant.controller;

import com.miele.laundry_management_system.model.common.transport.Meta;
import com.miele.laundry_management_system.model.tenant.entity.Tenant;
import com.miele.laundry_management_system.model.tenant.exception.TenantErrors;
import com.miele.laundry_management_system.model.tenant.service.TenantService;
import com.miele.laundry_management_system.model.tenant.transport.TenantDto;
import com.miele.laundry_management_system.model.tenant.transport.TenantDtoToTenantMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import java.util.Optional;
import java.util.UUID;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequestMapping("/api/v1/tenants")
@SecurityRequirement(name = "KeycloakOauth2Security")
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class TenantController {

  private static final String TENANT_DTO_API_EXAMPLE =
      "{\n" + "  \"name\": \"replace_me\",\n" + "  \"credentials\": \"replace_me\"\n" + "}";

  private final TenantService tenantService;
  private final TenantDtoToTenantMapper mapper;
  private final PagedResourcesAssembler<TenantDto> assembler;

  @GetMapping
  @PreAuthorize("hasAnyAuthority('ROLE_USER')")
  @Operation(description = "Returns a paginated list of tenants")
  public PagedModel<EntityModel<TenantDto>> findAll(
      @RequestParam("page") Optional<Integer> page,
      @RequestParam("pageSize") Optional<Integer> pageSize) {

    return assembler.toModel(
        tenantService.findAll(new Meta(page, pageSize)).map(this::convertToDto));
  }

  @GetMapping("{id}")
  @PreAuthorize("hasAnyAuthority('ROLE_USER')")
  @Operation(description = "Returns a particular tenant and its details")
  public TenantDto findById(
      @PathVariable("id") @Parameter(description = "The unique identifier (UUID) of the tenant")
          UUID id) {
    return tenantService
        .findById(id)
        .map(this::convertToDto)
        .orElseThrow(TenantErrors::tenantNotFoundException);
  }

  @PostMapping
  @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
  @Operation(description = "Creates a tenant")
  public TenantDto create(
      @Valid
          @io.swagger.v3.oas.annotations.parameters.RequestBody(
              description = "Tenant create Request body",
              content = @Content(examples = {@ExampleObject(value = TENANT_DTO_API_EXAMPLE)}),
              required = true)
          @RequestBody
          TenantDto tenantDto) {
    return convertToDto(tenantService.create(convertToEntity(tenantDto)));
  }

  @PutMapping("{id}")
  @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
  @Operation(description = "Updates a tenant")
  public TenantDto update(
      @PathVariable("id")
          @Parameter(
              description = "The unique identifier (UUID) of the appliance we want to update")
          UUID id,
      @Valid
          @io.swagger.v3.oas.annotations.parameters.RequestBody(
              description = "Tenant update Request body",
              content = @Content(examples = {@ExampleObject(value = TENANT_DTO_API_EXAMPLE)}),
              required = true)
          @RequestBody
          TenantDto tenantDto) {
    return convertToDto(tenantService.update(id, convertToEntity(tenantDto)));
  }

  @DeleteMapping("{id}")
  @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
  @Operation(description = "Removes a tenant")
  public @ResponseBody void delete(
      @PathVariable("id")
          @Parameter(
              description = "The unique identifier (UUID) of the tenant that we want ro remove")
          UUID id) {
    tenantService.delete(id);
  }

  private TenantDto convertToDto(Tenant tenant) {
    return mapper.entityToDto(tenant);
  }

  private Tenant convertToEntity(TenantDto tenantDto) {
    return mapper.dtoToEntity(tenantDto);
  }
}
