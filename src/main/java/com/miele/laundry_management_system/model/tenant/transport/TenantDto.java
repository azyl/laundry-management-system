package com.miele.laundry_management_system.model.tenant.transport;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import java.util.UUID;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldDefaults;
import org.springframework.hateoas.RepresentationModel;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode(callSuper = true)
public class TenantDto extends RepresentationModel<TenantDto> {
  UUID id;

  @NotBlank
  @Size(min = 3, max = 50)
  String name;

  @NotBlank
  @Size(max = 255)
  @JsonProperty(access = Access.WRITE_ONLY)
  String credentials;
}
