package com.miele.laundry_management_system.model.tenant.multitenancy;

import javax.persistence.EntityManager;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.hibernate.Session;

@Aspect
public class FilterByTenant {

  @AfterReturning(
    pointcut = "bean(entityManagerFactory) && execution(* createEntityManager(..))",
    returning = "retVal"
  )
  public void afterEnitityManagerCreation(JoinPoint joinPoint, Object retVal) {
    TenantContext.getCurrentTenantId()
        .ifPresent(
            tenantId -> {
              if (retVal instanceof EntityManager) {
                Session session = ((EntityManager) retVal).unwrap(Session.class);
                session.enableFilter("filterByTenant").setParameter("tenantId", tenantId);
              }
            });
  }
}
