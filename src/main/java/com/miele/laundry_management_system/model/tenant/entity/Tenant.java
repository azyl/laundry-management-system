package com.miele.laundry_management_system.model.tenant.entity;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.hateoas.RepresentationModel;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class Tenant extends RepresentationModel<Tenant> {

  @Id
  @GeneratedValue(generator = "uuid2")
  @GenericGenerator(name = "uuid2", strategy = "uuid2")
  private UUID id;

  @Column(nullable = false, length = 50)
  private String name;

  @Column(nullable = false)
  private String credentials;
}
