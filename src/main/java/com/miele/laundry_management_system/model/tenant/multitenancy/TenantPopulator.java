package com.miele.laundry_management_system.model.tenant.multitenancy;

import com.miele.laundry_management_system.model.tenant.entity.Tenant;
import javax.persistence.EntityManager;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class TenantPopulator implements ApplicationContextAware {

  private ApplicationContext applicationContext;

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.applicationContext = applicationContext;
  }

  @PrePersist
  @PreUpdate
  public void populateTenant(Object entity) {
    if (!(entity instanceof TenantAccess)) {
      return;
    }

    TenantContext.getCurrentTenantId()
        .ifPresent(
            tenantId ->
                ((TenantAccess) entity)
                    .setTenant(
                        applicationContext
                            .getBean(EntityManager.class)
                            .getReference(Tenant.class, tenantId)));
  }
}
