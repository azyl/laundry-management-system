package com.miele.laundry_management_system.model.tenant.exception;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TenantErrors {
  public static HttpClientErrorException tenantNotFoundException() {
    return new HttpClientErrorException(HttpStatus.NOT_FOUND, "The specified tenant was not found");
  }
}
