package com.miele.laundry_management_system.model.tenant.multitenancy;

import java.util.Optional;
import java.util.UUID;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class TenantContext {

  private static final ThreadLocal<UUID> currentTenantId = new ThreadLocal<>();

  public static Optional<UUID> getCurrentTenantId() {
    return Optional.ofNullable(currentTenantId.get());
  }

  public static void setCurrentTenantId(UUID tenant) {
    currentTenantId.set(tenant);
  }

  public static void clear() {
    currentTenantId.remove();
  }
}
