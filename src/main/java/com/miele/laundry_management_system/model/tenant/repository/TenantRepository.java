package com.miele.laundry_management_system.model.tenant.repository;

import com.miele.laundry_management_system.model.tenant.entity.Tenant;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TenantRepository extends JpaRepository<Tenant, UUID> {}
