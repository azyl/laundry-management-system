package com.miele.laundry_management_system.model.tenant.multitenancy;

import com.miele.laundry_management_system.model.tenant.entity.Tenant;

public interface TenantAccess {

  String TENANT_ID = "tenant_id";

  Tenant getTenant();

  void setTenant(Tenant tenant);
}
