package com.miele.laundry_management_system.model.tenant.transport;

import com.miele.laundry_management_system.model.tenant.entity.Tenant;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TenantDtoToTenantMapper {

  Tenant dtoToEntity(TenantDto dto);

  TenantDto entityToDto(Tenant entity);
}
