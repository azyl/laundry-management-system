package com.miele.laundry_management_system.model.common.operations;

public interface IdAccess<T> {

  T getId();
}
