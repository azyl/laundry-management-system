package com.miele.laundry_management_system.model.common.entity;

import com.miele.laundry_management_system.model.common.operations.IdAccess;
import java.util.UUID;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

@Getter
@Setter
@MappedSuperclass
public class EntityWithTenantAndUuid extends EntityWithTenant implements IdAccess<UUID> {
  @Id
  @GeneratedValue(generator = "uuid2")
  @GenericGenerator(name = "uuid2", strategy = "uuid2")
  private UUID id;
}
