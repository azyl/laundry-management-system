package com.miele.laundry_management_system.model.common.entity;

import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
public abstract class EntityWithTenantAndUuidAndLastModifiedAndCreatedDate
    extends EntityWithTenantAndUuidAndLastModified {

  @Column(nullable = false, updatable = false)
  private ZonedDateTime createdDate;

  @PrePersist
  public void setCreatedDate() {
    createdDate = ZonedDateTime.now();
  }
}
