package com.miele.laundry_management_system.model.common.transport;

import java.util.Optional;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Meta {

  private static final int DEFAULT_PAGE = 0;
  private static final int DEFAULT_PAGE_SIZE = 100;

  private Integer page;
  private Integer pageSize;
  private Integer totalElements;
  private Integer totalPages;

  public Meta(Optional<Integer> page, Optional<Integer> pageSize) {
    this.page = page.orElse(DEFAULT_PAGE);
    this.pageSize = pageSize.orElse(DEFAULT_PAGE_SIZE);
  }
}
