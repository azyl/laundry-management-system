package com.miele.laundry_management_system.model.common.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.miele.laundry_management_system.model.tenant.entity.Tenant;
import com.miele.laundry_management_system.model.tenant.multitenancy.TenantAccess;
import com.miele.laundry_management_system.model.tenant.multitenancy.TenantPopulator;
import com.miele.laundry_management_system.model.tenant.multitenancy.TenantValidator;
import javax.persistence.EntityListeners;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;
import org.springframework.hateoas.RepresentationModel;


@Data
@MappedSuperclass
@EqualsAndHashCode(callSuper = true)
@FilterDef(
    name = "filterByTenant",
    parameters = {@ParamDef(name = "tenantId", type = "java.util.UUID")})
@Filters({@Filter(name = "filterByTenant", condition = ":tenantId = tenant_id")})
@EntityListeners({TenantPopulator.class, TenantValidator.class })
public class EntityWithTenant extends RepresentationModel<EntityWithTenant>
    implements TenantAccess {

  @ManyToOne
  @JoinColumn(
      name = TENANT_ID,
      foreignKey = @ForeignKey(name = "fk_entity_with_tenant_tenant"),
      nullable = false)
  @JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "credentials"})
  private Tenant tenant;
}
