package com.miele.laundry_management_system.model.common.transport;

public interface CustomHeaders {
  String USER_ID_HEADER = "X-User-Id";
  String TENANT_ID_HEADER = "X-Tenant-Id";
}
