package com.miele.laundry_management_system.model.common.entity;

import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
public abstract class EntityWithTenantAndUuidAndLastModified extends EntityWithTenantAndUuid {

  @Column(nullable = false)
  private ZonedDateTime lastModified;

  @PrePersist
  @PreUpdate
  public void updateLastModified() {
    lastModified = ZonedDateTime.now();
  }
}
