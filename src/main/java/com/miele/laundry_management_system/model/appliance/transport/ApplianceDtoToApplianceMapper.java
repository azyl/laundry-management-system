package com.miele.laundry_management_system.model.appliance.transport;

import com.miele.laundry_management_system.model.appliance.entity.Appliance;
import com.miele.laundry_management_system.model.appliance.entity.Appliance.Type;
import com.miele.laundry_management_system.model.appliance.entity.DryingAppliance;
import com.miele.laundry_management_system.model.appliance.entity.WashingAppliance;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ApplianceDtoToApplianceMapper {

  default Appliance dtoToEntity(ApplianceDto dto) {
    if (dto == null) {
      return null;
    } else if (dto.getType() == Type.WASHING_APPLIANCE) {
      return fromWashingApplianceDto((WashingApplianceDto) dto);
    } else if (dto.getType() == Type.DRYING_APPLIANCE) {
      return fromDryApplianceDto((DryingApplianceDto) dto);
      }
    return null;
  }

  default ApplianceDto entityToDto(Appliance entity) {
    if (entity == null) {
      return null;
    } else if (entity.getType() == Type.WASHING_APPLIANCE) {
      return fromWashingAppliance((WashingAppliance) entity);
    } else if (entity.getType() == Type.DRYING_APPLIANCE) {
      return fromDryAppliance((DryingAppliance) entity);
    }
    return null;
  }


  WashingAppliance fromWashingApplianceDto(WashingApplianceDto dto);
  DryingAppliance fromDryApplianceDto(DryingApplianceDto dto);

  WashingApplianceDto fromWashingAppliance(WashingAppliance dto);
  DryingApplianceDto fromDryAppliance(DryingAppliance dto);
}
