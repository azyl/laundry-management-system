package com.miele.laundry_management_system.model.appliance.transport;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class DryingApplianceDto extends ApplianceDto{

  String dryingSpecificAttribute;
}
