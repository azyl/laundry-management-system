package com.miele.laundry_management_system.model.appliance.controller;

import static com.miele.laundry_management_system.model.appliance.entity.Appliance.Type.Values.DRYING_APPLIANCE_NAME;
import static com.miele.laundry_management_system.model.appliance.entity.Appliance.Type.Values.WASHING_APPLIANCE_NAME;

import com.miele.laundry_management_system.model.appliance.entity.Appliance;
import com.miele.laundry_management_system.model.appliance.exception.ApplianceErrors;
import com.miele.laundry_management_system.model.appliance.service.ApplianceService;
import com.miele.laundry_management_system.model.appliance.transport.ApplianceDto;
import com.miele.laundry_management_system.model.appliance.transport.ApplianceDtoToApplianceMapper;
import com.miele.laundry_management_system.model.common.transport.Meta;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import java.util.Optional;
import java.util.UUID;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/appliances")
@SecurityRequirement(name = "KeycloakOauth2Security")
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class ApplianceController {
  private static final String WASHING_MACHINE_DTO_API_EXAMPLE =
      "{\n"
          + "  \"tenant\": {\n"
          + "    \"id\": \"valid_existing_tenant_uuid\"\n"
          + "  },\n"
          + "  \"name\": \"replace_me\",\n"
          + "  \"description\": \"replace_me\",\n"
          + "  \"type\": \"WASHING_APPLIANCE\",\n"
          + "  \"washingSpecificAttribute\": \"replace_me\"\n"
          + "}";

  private static final String DRYING_MACHINE_DTO_API_EXAMPLE =
      "{\n"
          + "  \"tenant\": {\n"
          + "    \"id\": \"valid_existing_tenant_uuid\"\n"
          + "  },\n"
          + "  \"name\": \"replace_me\",\n"
          + "  \"description\": \"replace_me\",\n"
          + "  \"type\": \"DRYING_APPLIANCE\",\n"
          + "  \"dryingSpecificAttribute\": \"replace_me\"\n"
          + "}";

  private final ApplianceService applianceService;
  private final ApplianceDtoToApplianceMapper mapper;
  private final PagedResourcesAssembler<ApplianceDto> assembler;

  @GetMapping
  @PreAuthorize("hasAnyAuthority('ROLE_USER')")
  @Operation(description = "Returns a paginated list of appliances")
  public PagedModel<EntityModel<ApplianceDto>> findAll(
      @RequestParam("page") Optional<Integer> page,
      @RequestParam("pageSize") Optional<Integer> pageSize) {
    return assembler.toModel(
        applianceService.findAll(new Meta(page, pageSize)).map(this::convertToDto));
  }

  @GetMapping("{id}")
  @PreAuthorize("hasAnyAuthority('ROLE_USER')")
  @Operation(description = "Returns a particular appliance and its details")
  public ApplianceDto findById(
      @PathVariable("id") @Parameter(description = "The unique identifier (UUID) of the appliance")
          UUID id) {

    return convertToDto(
        applianceService.findById(id).orElseThrow(ApplianceErrors::applianceNotFoundException));
  }

  @PostMapping
  @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
  @Operation(description = "Creates an appliance of the provided type")
  public ApplianceDto create(
      @Valid
          @io.swagger.v3.oas.annotations.parameters.RequestBody(
              description = "Appliance update Request body",
              content =
                  @Content(
                      examples = {
                        @ExampleObject(
                            name = WASHING_APPLIANCE_NAME,
                            summary = WASHING_APPLIANCE_NAME,
                            value = WASHING_MACHINE_DTO_API_EXAMPLE),
                        @ExampleObject(
                            name = DRYING_APPLIANCE_NAME,
                            summary = DRYING_APPLIANCE_NAME,
                            value = DRYING_MACHINE_DTO_API_EXAMPLE)
                      }),
              required = true)
          @RequestBody
          ApplianceDto applianceDto) {
    return convertToDto(applianceService.create(convertToEntity(applianceDto)));
  }

  @PutMapping("{id}")
  @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
  @Operation(
      description = "Updates an appliance",
      security = {@SecurityRequirement(name = "KeycloakOauth2Security")})
  public ApplianceDto update(
      @PathVariable("id")
          @Parameter(
              description = "The unique identifier (UUID) of the appliance we want to update")
          UUID id,
      @Valid
          @io.swagger.v3.oas.annotations.parameters.RequestBody(
              description = "Appliance update Request body",
              content =
                  @Content(
                      examples = {
                        @ExampleObject(
                            name = WASHING_APPLIANCE_NAME,
                            summary = WASHING_APPLIANCE_NAME,
                            value = WASHING_MACHINE_DTO_API_EXAMPLE),
                        @ExampleObject(
                            name = DRYING_APPLIANCE_NAME,
                            summary = DRYING_APPLIANCE_NAME,
                            value = DRYING_MACHINE_DTO_API_EXAMPLE)
                      }),
              required = true)
          @RequestBody
          ApplianceDto applianceDto) {
    return convertToDto(applianceService.update(id, convertToEntity(applianceDto)));
  }

  @DeleteMapping("{id}")
  @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
  @Operation(description = "Removes an appliance")
  public @ResponseBody void delete(
      @PathVariable("id")
          @Parameter(
              description = "The unique identifier (UUID) of the appliance that we want ro remove")
          UUID id) {
    applianceService.delete(id);
  }

  private ApplianceDto convertToDto(Appliance appliance) {
    return mapper.entityToDto(appliance);
  }

  private Appliance convertToEntity(ApplianceDto applianceDto) {
    return mapper.dtoToEntity(applianceDto);
  }
}
