package com.miele.laundry_management_system.model.appliance.entity;

import static com.miele.laundry_management_system.model.appliance.entity.Appliance.Type.Values.*;
import static javax.persistence.DiscriminatorType.STRING;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import com.miele.laundry_management_system.model.common.entity.EntityWithTenantAndUuidAndLastModifiedAndCreatedDate;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    property = "type",
    include = JsonTypeInfo.As.EXISTING_PROPERTY,
    visible = true)
@JsonSubTypes({
  @JsonSubTypes.Type(value = WashingAppliance.class, name = WASHING_APPLIANCE_NAME),
  @JsonSubTypes.Type(value = DryingAppliance.class, name = DRYING_APPLIANCE_NAME),
})
@DiscriminatorColumn(name = "TYPE", discriminatorType = STRING, length = 20)
@NoArgsConstructor
public abstract class Appliance extends EntityWithTenantAndUuidAndLastModifiedAndCreatedDate {

  @Column(nullable = false, length = 50)
  private String name;

  private String description;

  @Column(name = "TYPE", nullable = false, insertable = false, updatable = false, length = 20)
  @Enumerated(EnumType.STRING)
  private Type type;

  public enum Type {
    WASHING_APPLIANCE(WASHING_APPLIANCE_NAME),
    DRYING_APPLIANCE(DRYING_APPLIANCE_NAME);

    Type(String val) {
      // force equality between name of enum instance, and value of constant
      if (!this.name().equals(val))
        throw new IllegalArgumentException("Incorrect use of Appliance Type");
    }

    public static class Values {

      private Values() {}

      public static final String WASHING_APPLIANCE_NAME = "WASHING_APPLIANCE";
      public static final String DRYING_APPLIANCE_NAME = "DRYING_APPLIANCE";
    }
  }
}
