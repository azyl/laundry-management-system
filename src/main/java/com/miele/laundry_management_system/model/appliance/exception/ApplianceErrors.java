package com.miele.laundry_management_system.model.appliance.exception;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ApplianceErrors {
  public static HttpClientErrorException applianceNotFoundException() {
    return new HttpClientErrorException(HttpStatus.NOT_FOUND, "The specified appliance was not found");
  }
}
