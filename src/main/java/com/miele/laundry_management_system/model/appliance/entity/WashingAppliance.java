package com.miele.laundry_management_system.model.appliance.entity;

import static com.miele.laundry_management_system.model.appliance.entity.Appliance.Type.Values.WASHING_APPLIANCE_NAME;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
@DiscriminatorValue(WASHING_APPLIANCE_NAME)
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
public class WashingAppliance extends Appliance {

  @Column(name = "WASHING_SPECIFIC_ATTRIBUTE")
  String washingSpecificAttribute;
}
