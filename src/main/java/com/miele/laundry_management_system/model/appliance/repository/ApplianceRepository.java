package com.miele.laundry_management_system.model.appliance.repository;

import com.miele.laundry_management_system.model.appliance.entity.Appliance;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplianceRepository extends JpaRepository<Appliance, UUID> {}
