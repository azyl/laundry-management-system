package com.miele.laundry_management_system.model.appliance.transport;

import static com.miele.laundry_management_system.model.appliance.entity.Appliance.Type.Values.DRYING_APPLIANCE_NAME;
import static com.miele.laundry_management_system.model.appliance.entity.Appliance.Type.Values.WASHING_APPLIANCE_NAME;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.miele.laundry_management_system.model.appliance.entity.Appliance.Type;
import com.miele.laundry_management_system.model.tenant.transport.TenantDto;
import java.util.UUID;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.hateoas.RepresentationModel;

@Data
@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    property = "type",
    include = JsonTypeInfo.As.EXISTING_PROPERTY,
    visible = true)
@JsonSubTypes({
    @JsonSubTypes.Type(value = WashingApplianceDto.class, name = WASHING_APPLIANCE_NAME),
    @JsonSubTypes.Type(value = DryingApplianceDto.class, name = DRYING_APPLIANCE_NAME),
})
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public abstract class ApplianceDto extends RepresentationModel<ApplianceDto> {
  UUID id;

  @NotNull TenantDto tenant;

  @NotBlank
  @Size(min = 3, max = 50)
  String name;

  @Size(max = 255)
  String description;

  @NotNull Type type;
}
