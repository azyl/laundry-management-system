package com.miele.laundry_management_system.model.appliance.service;

import com.miele.laundry_management_system.model.appliance.entity.Appliance;
import com.miele.laundry_management_system.model.appliance.repository.ApplianceRepository;
import com.miele.laundry_management_system.model.common.transport.Meta;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor_ = {@Autowired})
public class ApplianceService {

  private final ApplianceRepository applianceRepository;

  public Page<Appliance> findAll(Meta meta) {
    return applianceRepository.findAll(PageRequest.of(meta.getPage(), meta.getPageSize()));
  }

  public Optional<Appliance> findById(UUID id) {
    return applianceRepository.findById(id);
  }

  public Appliance create(Appliance appliance) {
    appliance.setId(null);
    return applianceRepository.save(appliance);
  }

  public Appliance update(UUID id, Appliance appliance) {
    appliance.setId(id);
    return applianceRepository.save(appliance);
  }

  public void delete(UUID id) {
    applianceRepository.deleteById(id);
  }
}
