package com.miele.laundry_management_system.model.tenant.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mockingDetails;
import static org.mockito.Mockito.when;

import com.miele.laundry_management_system.model.tenant.entity.Tenant;
import com.miele.laundry_management_system.model.tenant.repository.TenantRepository;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@RunWith(MockitoJUnitRunner.class)
class TenantServiceTest {

  @Mock private TenantRepository tenantRepository;

  @InjectMocks private TenantService tenantService;

  @BeforeEach
  void setUp() {}

  @AfterEach
  void tearDown() {}

  @Test
  void findAll() {}

  @Test
  void findById() {
    UUID id = UUID.randomUUID();

    Tenant tenant = new Tenant();
    tenant.setId(id);

    // did we properly mocked the repository
    assertTrue(mockingDetails(tenantRepository).isMock());

    when(tenantRepository.findById(any(UUID.class))).thenReturn(Optional.of(tenant));

    assertEquals(Optional.of(tenant), tenantService.findById(id));
  }

  @Test
  void create() {
    UUID id = UUID.randomUUID();

    Tenant tenant = new Tenant();
    tenant.setId(id);

    // did we properly mocked the repository
    assertTrue(mockingDetails(tenantRepository).isMock());
    when(tenantRepository.save(tenant)).thenReturn(tenant);

    assertEquals(tenant, tenantService.create(tenant));
  }
}
