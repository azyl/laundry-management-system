package com.miele.laundry_management_system;

import static org.assertj.core.api.Assertions.assertThat;

import com.miele.laundry_management_system.model.tenant.entity.Tenant;
import com.miele.laundry_management_system.model.tenant.repository.TenantRepository;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
//@SpringBootTest
@DataJpaTest()
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class LaundryManagementSystemApplicationTests {

  @Autowired TestEntityManager entityManager;
  @Autowired TenantRepository tenantRepository;

  @Test
  void should_save_user() {
    Tenant tenant = new Tenant();
    tenant.setName("test");
    tenant.setCredentials("cred");

    tenant = entityManager.persistAndFlush(tenant);
    assertThat(tenantRepository.findById(tenant.getId()).orElseThrow()).isEqualTo(tenant);
  }

  @Test
  void should_delete_user() {
    Tenant tenant = new Tenant();
    tenant.setName("test");
    tenant.setCredentials("cred");

    tenant = entityManager.persistAndFlush(tenant);
    tenantRepository.deleteById(tenant.getId());
    assertThat(tenantRepository.findById(tenant.getId())).isNotPresent();
  }
}
