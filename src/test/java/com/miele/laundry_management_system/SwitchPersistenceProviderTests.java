package com.miele.laundry_management_system;

import com.miele.laundry_management_system.model.tenant.repository.TenantRepository;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import static org.assertj.core.api.Assertions.assertThat;

@Disabled
@DataJpaTest
@ActiveProfiles("integration-test")
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class SwitchPersistenceProviderTests {

  @Autowired private TenantRepository tenantRepository;

  @Test
  void databaseMigrationOk() {
    var tenants = tenantRepository.findAll();
    assertThat(tenants.size()).isZero();
  }
}
